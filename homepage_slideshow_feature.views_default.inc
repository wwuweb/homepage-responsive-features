<?php
/**
 * @file
 * homepage_slideshow_feature.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function homepage_slideshow_feature_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'homepage_slideshow';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Homepage Slideshow';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '6';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'flexslider';
  $handler->display->display_options['style_options']['optionset'] = 'homepage_settings';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Caption */
  $handler->display->display_options['fields']['field_slideshow_url_1']['id'] = 'field_slideshow_url_1';
  $handler->display->display_options['fields']['field_slideshow_url_1']['table'] = 'field_data_field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url_1']['field'] = 'field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url_1']['ui_name'] = 'Caption';
  $handler->display->display_options['fields']['field_slideshow_url_1']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_url_1']['element_class'] = 'caption';
  $handler->display->display_options['fields']['field_slideshow_url_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_url_1']['click_sort_column'] = 'url';
  /* Field: Link for Image */
  $handler->display->display_options['fields']['field_slideshow_url']['id'] = 'field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['table'] = 'field_data_field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['field'] = 'field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['ui_name'] = 'Link for Image';
  $handler->display->display_options['fields']['field_slideshow_url']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_url']['element_wrapper_type'] = 'div';
  $handler->display->display_options['fields']['field_slideshow_url']['element_wrapper_class'] = 'caption';
  $handler->display->display_options['fields']['field_slideshow_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_slideshow_url']['type'] = 'link_plain';
  /* Field: Content: Slideshow Image */
  $handler->display->display_options['fields']['field_story_image']['id'] = 'field_story_image';
  $handler->display->display_options['fields']['field_story_image']['table'] = 'field_data_field_story_image';
  $handler->display->display_options['fields']['field_story_image']['field'] = 'field_story_image';
  $handler->display->display_options['fields']['field_story_image']['label'] = '';
  $handler->display->display_options['fields']['field_story_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_story_image']['alter']['text'] = '<a href="[field_slideshow_url]">[field_story_image]</a>';
  $handler->display->display_options['fields']['field_story_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_story_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_story_image']['type'] = 'picture';
  $handler->display->display_options['fields']['field_story_image']['settings'] = array(
    'picture_mapping' => 'slideshow_group',
    'fallback_image_style' => '',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_story_image']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_story_image']['field_api_classes'] = TRUE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'slideshow' => 'slideshow',
  );

  /* Display: Current Stories */
  $handler = $view->new_display('page', 'Current Stories', 'current_stories');
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['path'] = 'stories';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'Homepage Slideshow';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'menu-flexslider-example';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: admin list of content */
  $handler = $view->new_display('page', 'admin list of content', 'page_2');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = '';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_slideshow_url' => 'field_slideshow_url',
    'field_story_image' => 'field_story_image',
    'title' => 'title',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'field_slideshow_url' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'field_story_image' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Story URL */
  $handler->display->display_options['fields']['field_slideshow_url']['id'] = 'field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['table'] = 'field_data_field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['field'] = 'field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_url']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_slideshow_url']['type'] = 'plain';
  $handler->display->display_options['fields']['field_slideshow_url']['group_column'] = 'url';
  /* Field: Content: Slideshow Image */
  $handler->display->display_options['fields']['field_story_image']['id'] = 'field_story_image';
  $handler->display->display_options['fields']['field_story_image']['table'] = 'field_data_field_story_image';
  $handler->display->display_options['fields']['field_story_image']['field'] = 'field_story_image';
  $handler->display->display_options['fields']['field_story_image']['label'] = '';
  $handler->display->display_options['fields']['field_story_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_story_image']['alter']['path'] = '[field_slideshow_url]';
  $handler->display->display_options['fields']['field_story_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_story_image']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_story_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_story_image']['settings'] = array(
    'image_style' => 'thumbnail',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_story_image']['group_column'] = 'fid';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_slideshow_url] ';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Field: Content: Published */
  $handler->display->display_options['fields']['status']['id'] = 'status';
  $handler->display->display_options['fields']['status']['table'] = 'node';
  $handler->display->display_options['fields']['status']['field'] = 'status';
  $handler->display->display_options['fields']['status']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['status']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['status']['not'] = 0;
  /* Field: Content: Order */
  $handler->display->display_options['fields']['field_weight']['id'] = 'field_weight';
  $handler->display->display_options['fields']['field_weight']['table'] = 'field_data_field_weight';
  $handler->display->display_options['fields']['field_weight']['field'] = 'field_weight';
  $handler->display->display_options['fields']['field_weight']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_weight']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Order (field_weight) */
  $handler->display->display_options['sorts']['field_weight_value']['id'] = 'field_weight_value';
  $handler->display->display_options['sorts']['field_weight_value']['table'] = 'field_data_field_weight';
  $handler->display->display_options['sorts']['field_weight_value']['field'] = 'field_weight_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $handler->display->display_options['path'] = 'admin-stories';

  /* Display: Story Archive */
  $handler = $view->new_display('page', 'Story Archive', 'page_3');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Story Archive';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'grid';
  $handler->display->display_options['style_options']['grouping'] = '';
  $handler->display->display_options['style_options']['columns'] = '1';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['inline'] = array(
    'field_story_image' => 'field_story_image',
    'title' => 'title',
  );
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Story URL */
  $handler->display->display_options['fields']['field_slideshow_url']['id'] = 'field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['table'] = 'field_data_field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['field'] = 'field_slideshow_url';
  $handler->display->display_options['fields']['field_slideshow_url']['label'] = '';
  $handler->display->display_options['fields']['field_slideshow_url']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_slideshow_url']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_url']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_slideshow_url']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_slideshow_url']['type'] = 'plain';
  $handler->display->display_options['fields']['field_slideshow_url']['group_column'] = 'url';
  /* Field: Content: Slideshow Image */
  $handler->display->display_options['fields']['field_story_image']['id'] = 'field_story_image';
  $handler->display->display_options['fields']['field_story_image']['table'] = 'field_data_field_story_image';
  $handler->display->display_options['fields']['field_story_image']['field'] = 'field_story_image';
  $handler->display->display_options['fields']['field_story_image']['label'] = '';
  $handler->display->display_options['fields']['field_story_image']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_story_image']['alter']['path'] = '[field_slideshow_url]';
  $handler->display->display_options['fields']['field_story_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_story_image']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['field_story_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_story_image']['settings'] = array(
    'image_style' => 'slideshow_archive',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_story_image']['group_column'] = 'fid';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['title']['alter']['path'] = '[field_slideshow_url] ';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Story Archive Date (field_slideshow_archive) */
  $handler->display->display_options['sorts']['field_slideshow_archive_value']['id'] = 'field_slideshow_archive_value';
  $handler->display->display_options['sorts']['field_slideshow_archive_value']['table'] = 'field_data_field_slideshow_archive';
  $handler->display->display_options['sorts']['field_slideshow_archive_value']['field'] = 'field_slideshow_archive_value';
  $handler->display->display_options['sorts']['field_slideshow_archive_value']['order'] = 'DESC';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = '0';
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'slideshow' => 'slideshow',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Story Archive Date (field_slideshow_archive) */
  $handler->display->display_options['filters']['field_slideshow_archive_value']['id'] = 'field_slideshow_archive_value';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['table'] = 'field_data_field_slideshow_archive';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['field'] = 'field_slideshow_archive_value';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['operator'] = '<';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['group'] = 1;
  $handler->display->display_options['filters']['field_slideshow_archive_value']['default_date'] = 'now';
  $handler->display->display_options['path'] = 'story-archive';

  /* Display: Archive Test */
  $handler = $view->new_display('page', 'Archive Test', 'page_4');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['style_options']['grouping'] = '';
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'slideshow' => 'slideshow',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Story Archive Date (field_slideshow_archive) */
  $handler->display->display_options['filters']['field_slideshow_archive_value']['id'] = 'field_slideshow_archive_value';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['table'] = 'field_data_field_slideshow_archive';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['field'] = 'field_slideshow_archive_value';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['operator'] = 'empty';
  $handler->display->display_options['filters']['field_slideshow_archive_value']['group'] = 1;
  $handler->display->display_options['path'] = 'archive-test';
  $export['homepage_slideshow'] = $view;

  return $export;
}
