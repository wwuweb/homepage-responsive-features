<?php
/**
 * @file
 * homepage_slideshow_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function homepage_slideshow_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function homepage_slideshow_feature_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_image_default_styles().
 */
function homepage_slideshow_feature_image_default_styles() {
  $styles = array();

  // Exported image style: homepage_slideshow.
  $styles['homepage_slideshow'] = array(
    'label' => 'homepage_slideshow',
    'effects' => array(
      12 => NULL,
    ),
  );

  // Exported image style: slide_breakpoints_theme_wwuzen_home_large_1x.
  $styles['slide_breakpoints_theme_wwuzen_home_large_1x'] = array(
    'label' => 'slide_breakpoints_theme_wwuzen_home_large_1x',
    'effects' => array(
      13 => NULL,
    ),
  );

  // Exported image style: slide_breakpoints_theme_wwuzen_home_small_1x.
  $styles['slide_breakpoints_theme_wwuzen_home_small_1x'] = array(
    'label' => 'slide_breakpoints_theme_wwuzen_home_small_1x',
    'effects' => array(
      14 => NULL,
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function homepage_slideshow_feature_node_info() {
  $items = array(
    'slideshow' => array(
      'name' => t('Slideshow'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Story Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
