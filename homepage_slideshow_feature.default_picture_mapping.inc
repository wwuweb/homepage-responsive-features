<?php
/**
 * @file
 * homepage_slideshow_feature.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function homepage_slideshow_feature_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Slideshow Group';
  $picture_mapping->machine_name = 'slideshow_group';
  $picture_mapping->breakpoint_group = FALSE;
  $picture_mapping->mapping = array();
  $export['slideshow_group'] = $picture_mapping;

  return $export;
}
